from django.db import models

# Create your models her
from django.db import models


class Engine(models.Model):
    name = models.CharField(max_length=25)

    def __unicode__(self):
        return self.name


class Car(models.Model):
    name = models.CharField(max_length=25)
    engine = models.OneToOneField(Engine)

    def __unicode__(self):
        return self.name


class Engineone(models.Model):
    name = models.CharField(max_length=25)

    def __unicode__(self):
        return self.name


class Carone(models.Model):
    name = models.CharField(max_length=25)
    engine = models.ForeignKey(Engineone, unique=True)

    def __unicode__(self):
        return self.name
